/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

var mm_console = []; // "";
    
var console_org = console;

console = {
  assert: function(a, msg){
    if(a){
      console_org.log(msg);
      // mm_console+=msg+"\n";
      mm_console.push(msg);
      if(mm_console1 && console1){
        console1.logln(typeof msg == "string" ? msg : msg.message);
        
      }
    }
  },
  log: function(msg){
    console_org.log(msg);
    // mm_console+=msg+"\n";
    mm_console.push(msg);
    if(mm_console1 && console1){
      console1.logln(typeof msg == "string" ? msg : msg.message);
    }
  },
  info: function(msg){
    console_org.info(msg);
    // mm_console+=msg+"\n";
    mm_console.push(msg);
    if(mm_console1 && console1){
      console1.log("Info: ");
      console1.logln(typeof msg == "string" ? msg : msg.message);
    }
  },
  error: function(msg){
    console_org.error(msg);
    // mm_console+=msg+"\n";
    mm_console.push(msg);
    if(mm_console1 && console1){
      //console1.logln(msg.stack || msg);
      console1.log("Error: ");
      console1.logln(typeof msg == "string" ? msg : msg.message);
      //console1.logln(typeof msg == "string" ? msg : msg.stack);
    }
  },
  warn: function(msg){
    console_org.warn(msg);
    // mm_console+=msg+"\n";
    mm_console.push(msg);
    if(mm_console1 && console1){
      //console1.logln(msg.stack || msg);
      console1.log("Error: ");
      console1.logln(typeof msg == "string" ? msg : msg.message);
      //console1.logln(typeof msg == "string" ? msg : msg.stack);
    }
  },
  trace: function(){
    console_org.trace();
  },
  count: function(label){
    console_org.count(label);
  },
  dir: function(obj){
    console_org.dir(obj);
  },
  group: function(){
    console_org.group();
  },
  groupCollapsed: function(){
    console_org.groupCollapsed();
  },
  groupEnd: function(){
    console_org.groupEnd();
  },
  time: function(name){
    console_org.time(name);
  },
  timeEnd: function(name){
    console_org.timeEnd(name);
  }
}