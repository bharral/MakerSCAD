/**
 * Copyright (c) 2018 Juan Carlos Orozco Arena. 
 * The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License V3.0 as published by
 * the Free Software Foundation
 *
 * Author: Juan Carlos Orozco
 * Includes contributions by MakerMex team: Luis Arturo Pacheco

 * 3D design software based on software distributed with MIT License:
 * Blockly
 * Threejs
 * Polymer 
 */

var mm_array_par = function(par, n){
  var ret = "";

  if(typeof par === "string"){
    if(par[0] == "["){
      var strip = par.slice(1,-1); //.split(",");
      ret = strip;
    }
    else{
      for(var i=0; i<n-1; i++){
        ret += par+"["+i+"], ";
      }
      ret += par+"["+(n-1)+"]";
    }
  }
  return ret;
}

Blockly.JavaScript['three_init'] = function(block) {
  var checkbox_lights = block.getFieldValue('lights') == 'TRUE';
  var checkbox_physijs = block.getFieldValue('physijs') == 'TRUE';
  var code =
    "mm_scene = null;\n";
  if(checkbox_physijs){
    code +=
      "mm_scene = new Physijs.Scene();\n" +
      "mm_scene.setGravity(new THREE.Vector3( 0, 0, -50 ));\n" + // TODO: Define default gravity value.
      "mm_scene.addEventListener( 'update', function() {\n" +
      "  if(mm_state.animate){\n" +
      "    mm_scene.simulate( undefined, 2 );\n" +
      "  }\n" +
      "});\n";
  } else {
    code += "mm_scene = new THREE.Scene();\n";
  }
  if(checkbox_lights){
    if(mm_three_toolbar.shadows){
      code +=
        "mm_scene.add(new THREE.AmbientLight('#ffffff',0.3));\n"+
        "mm_scene.add(mm_set(new THREE.PointLight('#ffffff', 50, 2000, 10)).setTranslation([400, 400, 400]));\n";
    } else {
      code +=
        "mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));\n";
    }
  }
  return code;
};

// TODO Check if we need to call dispose on some of this parts to avoid memory leaks
//Blockly.JavaScript['three_init'] = function(block) {
  //var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  //var code = "" +
    //"mm_scene = null;\n" +
    //"mm_scene = new THREE.Scene();\n" +
    //"mm_scene.add( mm_ambient );\n" +
    //"mm_scene.add(" + value_mesh + ");\n" +
    //"mm_renderer.render( mm_scene, mm_camera );\n";
  //return code;
//};

Blockly.JavaScript['three_new_scene'] = function(block) {
  var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  var code = "" +
    "mm_scene = null;\n" +
    "mm_scene = new THREE.Scene();\n"; // +
    //"mm_animation = animation;\n";
  return code;
};

Blockly.JavaScript['three_new_basic_scene'] = function(block) {
  var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  var code;
  if(mm_three_toolbar.shadows){
    code =
      "mm_scene = null;\n"+
      "mm_scene = new THREE.Scene();\n"+
      "mm_scene.add(new THREE.AmbientLight('#ffffff',0.3));\n"+
      "mm_scene.add(mm_set(new THREE.PointLight('#ffffff', 50, 2000, 10)).setTranslation([400, 400, 400]));\n";
  } else {
    code =
      "mm_scene = null;\n" +
      "mm_scene = new THREE.Scene();\n" +
      "mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));\n";
  }
  return code;
};

Blockly.JavaScript['physi_new_basic_scene'] = function(block) {
  var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  var code = "" +
    "mm_scene = null;\n" +
    "mm_scene = new Physijs.Scene();\n" +
    "mm_scene.setGravity(new THREE.Vector3( 0, 0, -50 ));\n" + // TODO: Define default gravity value.
    "mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));\n" +
    "mm_scene.addEventListener( 'update', function() {\n" +
    "  if(mm_state.animate){\n" +
    "    mm_scene.simulate( undefined, 2 );\n" +
    "  }\n" +
    "});\n";
    //"mm_animation = animation;\n";
  return code;
};

Blockly.JavaScript['three_new_group'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var code = "mm_set(new THREE.Object3D())";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_group'] = function(block) {
  // Create a list with any number of elements of any type.
  //  var value_chain = Blockly.JavaScript.valueToCode(block, 'chain', Blockly.JavaScript.ORDER_ATOMIC);
  var codeArr = new Array(block.itemCount_-1);
  for (var n = 1; n < block.itemCount_; n++) {
    // code[n] = Blockly.JavaScript.valueToCode(block, 'ADD' + n,
    //     Blockly.JavaScript.ORDER_COMMA) || 'null';
    // TODO: Fix the naming on the AddSubGroup block and use code above
    codeArr[n-1] = Blockly.JavaScript.valueToCode(block, 'items' + n,
        Blockly.JavaScript.ORDER_COMMA) || 'null';
  }
  //var chain = "";
  //if(value_chain != ""){
  //  chain = "."+value_chain.trim();
  //}
  //var code = text_name.substr(1, text_name.length-2) + '(' + codeArr.join(', ') + ')' + chain;
  var code = 'mm_set(new THREE.Object3D().addMeshList([' + codeArr.join(', ') + ']))';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
  //return code;
};

Blockly.JavaScript['three_animation'] = function(block) {
  //var variable_delta = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('delta'), Blockly.Variables.NAME_TYPE);
  var statements_st = Blockly.JavaScript.statementToCode(block, 'st');
  // TODO: Assemble JavaScript into code variable.
  var code = "" +
    "mm_animation = function(){\n" +
    "  var delta = mm_clock.getDelta();\n" +
    statements_st +
    "}";
  return code;
};

Blockly.JavaScript['three_play_init'] = function(block) {
  //var variable_delta = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('delta'), Blockly.Variables.NAME_TYPE);
  var statements_st = Blockly.JavaScript.statementToCode(block, 'st');
  // TODO: Assemble JavaScript into code variable.
  var code = "" +
    "mm_play_init = function(){\n" +
    statements_st +
    "}";
  return code;
};

Blockly.JavaScript['three_play_close'] = function(block) {
  //var variable_delta = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('delta'), Blockly.Variables.NAME_TYPE);
  var statements_st = Blockly.JavaScript.statementToCode(block, 'st');
  // TODO: Assemble JavaScript into code variable.
  var code = "" +
    "mm_play_close = function(){\n" +
    statements_st +
    "}";
  return code;
};

Blockly.JavaScript['three_draw'] = function(block) {
  //var variable_delta = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('delta'), Blockly.Variables.NAME_TYPE);
  var statements_st = Blockly.JavaScript.statementToCode(block, 'st');
  // TODO: Assemble JavaScript into code variable.
  var code = "" +
    "mm_draw = function(){\n" +
    statements_st +
    "}";
  return code;
};

Blockly.JavaScript['three_render'] = function(block) {
  var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  var code = "mm_renderer.render( mm_scene, mm_camera );\n";
  return code;
};

Blockly.JavaScript['three_add_mesh_list'] = function(block) {
  var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  //var value_block2 = Blockly.JavaScript.valueToCode(block, 'block2', Blockly.JavaScript.ORDER_ATOMIC);
  //var code = "(new ThreeBSP(" + value_block1 + "))." + dropdown_boolean_op + "(new ThreeBSP("+value_block2+")).toMesh(mm_material.clone())";
  //return [code, Blockly.JavaScript.ORDER_ATOMIC];

  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.JavaScript.valueToCode(block, 'ADD' + n,
        Blockly.JavaScript.ORDER_COMMA) || 'null';
  }
  code = value_mesh+'\n  .addMeshList([' + code.join(', ') + ']);\n';
  return code;
};

Blockly.JavaScript['three_scene_add'] = function(block) {
  var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  var code = "mm_scene.add("+value_mesh+");\n";
  return code;
};

Blockly.JavaScript['three_object_add'] = function(block) {
  var text_variable = block.getFieldValue('variable');
  var value_mesh = Blockly.JavaScript.valueToCode(block, 'mesh', Blockly.JavaScript.ORDER_ATOMIC);
  var code = text_variable+".add("+value_mesh+");\n";
  return code;
};

Blockly.JavaScript['three_light_ambient'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_intensity = Blockly.JavaScript.valueToCode(block, 'intensity', Blockly.JavaScript.ORDER_ATOMIC);
  if(value_color==""){
    var color = '0xFFFFFF';
  }
  else{
    var color = value_color;
  }
  if(value_intensity==""){
    var intensity = '0.2';
  }
  else{
    var intensity = value_intensity;
  }
  var code = "new THREE.AmbientLight("+color+","+intensity+")";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_light_directional'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_intensity = Blockly.JavaScript.valueToCode(block, 'intensity', Blockly.JavaScript.ORDER_ATOMIC);
  if(value_color==""){
    var color = '0xFFFFFF';
  }
  else{
    var color = value_color;
  }
  if(value_intensity==""){
    var intensity = '0.5';
  }
  else{
    var intensity = value_intensity;
  }
  var code = "mm_set(new THREE.DirectionalLight("+color+", "+intensity+"))";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_light_hemisphere'] = function(block) {
  var value_sky_color = Blockly.JavaScript.valueToCode(block, 'sky_color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_ground_color = Blockly.JavaScript.valueToCode(block, 'ground_color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_intensity = Blockly.JavaScript.valueToCode(block, 'intensity', Blockly.JavaScript.ORDER_ATOMIC);
  if(value_sky_color==""){
    var sky_color = '0xFFFFFF';
  }
  else{
    var sky_color = value_sky_color;
  }
  if(value_ground_color==""){
    var ground_color = '0x555555';
  }
  else{
    var ground_color = value_ground_color;
  }
  if(value_intensity==""){
    var intensity = '0.5';
  }
  else{
    var intensity = value_intensity;
  }
  var code = "new THREE.HemisphereLight("+sky_color+", "+ground_color+", "+intensity+")";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_light_point'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_intensity = Blockly.JavaScript.valueToCode(block, 'intensity', Blockly.JavaScript.ORDER_ATOMIC);
  var value_distance = Blockly.JavaScript.valueToCode(block, 'distance', Blockly.JavaScript.ORDER_ATOMIC);
  var value_decay = Blockly.JavaScript.valueToCode(block, 'decay', Blockly.JavaScript.ORDER_ATOMIC);
  if(value_color==""){
    var color = '0xFFFFFF';
  }
  else{
    var color = value_color;
  }
  if(value_distance==""){
    var distance = '100';
  }
  else{
    var distance = value_distance;
  }
  if(value_intensity==""){
    var intensity = '5';
  }
  else{
    var intensity = value_intensity;
  }
  if(value_decay==""){
    var decay = '10';
  }
  else{
    var decay = value_decay;
  }
  var code = "mm_set(new THREE.PointLight("+color+", "+intensity+", "+distance+", "+decay+"))";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_light_spot'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_intensity = Blockly.JavaScript.valueToCode(block, 'intensity', Blockly.JavaScript.ORDER_ATOMIC);
  var value_distance = Blockly.JavaScript.valueToCode(block, 'distance', Blockly.JavaScript.ORDER_ATOMIC);
  var value_angle = Blockly.JavaScript.valueToCode(block, 'angle', Blockly.JavaScript.ORDER_ATOMIC);
  var value_penumbra = Blockly.JavaScript.valueToCode(block, 'penumbra', Blockly.JavaScript.ORDER_ATOMIC);
  var value_decay = Blockly.JavaScript.valueToCode(block, 'decay', Blockly.JavaScript.ORDER_ATOMIC);
  if(value_color==""){
    var color = '0xFFFFFF';
  }
  else{
    var color = value_color;
  }
  if(value_distance==""){
    var distance = '500';
  }
  else{
    var distance = value_distance;
  }
  if(value_intensity==""){
    var intensity = '1';
  }
  else{
    var intensity = value_intensity;
  }
  if(value_decay==""){
    var decay = '1';
  }
  else{
    var decay = value_decay;
  }
  if(value_angle==""){
    var angle = '0.5';
  }
  else{
    var angle = value_angle;
  }
  if(value_penumbra==""){
    var penumbra = '0.5';
  }
  else{
    var penumbra = value_penumbra;
  }
  var code = "mm_set(new THREE.SpotLight("+color+", "+intensity+", "+distance+", "+angle+", "+penumbra+", "+decay+"))";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_part'] = function(block) {
  var value_block1 = Blockly.JavaScript.valueToCode(block, 'block1', Blockly.JavaScript.ORDER_ATOMIC);
  var dropdown_boolean_op = block.getFieldValue('boolean_op');
  var value_block2 = Blockly.JavaScript.valueToCode(block, 'block2', Blockly.JavaScript.ORDER_ATOMIC);
  //var code = "mm_set((new ThreeBSP(\n  " + value_block1 + "))\n  ." + dropdown_boolean_op + "(new ThreeBSP("+value_block2+"))\n  .toMesh(mm_material.clone()))";
  var code = "mm_boolean_op('"+dropdown_boolean_op+"', "+value_block1+", "+value_block2+")";
  //.geometry.computeVertexNormals()";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_box'] = function(block) {
  var value_size = Blockly.JavaScript.valueToCode(block, 'size', Blockly.JavaScript.ORDER_ATOMIC);
  var value_segments = Blockly.JavaScript.valueToCode(block, 'segments', Blockly.JavaScript.ORDER_ATOMIC);
  code = "mm_new_mesh(new THREE.BoxGeometry( " + mm_array_par(value_size,3) + ", "+value_segments+", "+value_segments+", "+value_segments+" ))";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_sphere'] = function(block) {
  var value_diameter = Blockly.JavaScript.valueToCode(block, 'diameter', Blockly.JavaScript.ORDER_ATOMIC);
  var value_resolution = Blockly.JavaScript.valueToCode(block, 'resolution', Blockly.JavaScript.ORDER_ATOMIC);
  code = "mm_new_mesh(new THREE.SphereGeometry( " + value_diameter + "/2, " + value_resolution + ", " + value_resolution + " ))";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_cylinder'] = function(block) {
  var value_diameter1 = Blockly.JavaScript.valueToCode(block, 'diameter1', Blockly.JavaScript.ORDER_ATOMIC);
  var value_diameter2 = Blockly.JavaScript.valueToCode(block, 'diameter2', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  //var value_center = block.getFieldValue('center');
  var value_center = 'true';
  var value_rounded = block.getFieldValue('rounded');
  var value_sides = Blockly.JavaScript.valueToCode(block, 'sides', Blockly.JavaScript.ORDER_ATOMIC);

  if(value_diameter1==""){
    var diameter1 = "10"; // Default value
  }
  else {
    var diameter1 = value_diameter1;
  }
  if(value_diameter2==""){
    var diameter2 = "10"; // Default value
  }
  else {
    var diameter2 = value_diameter2;
  }
  if(value_height==""){
    var height = "100"; // Default value
  }
  else {
    var height = value_height;
  }
  if(value_sides==""){
    var sides = "16"; // Default value
  }
  else {
    var sides = value_sides;
  }
  // var code = 'cylinder({r1: '+diameter1+'/2, r2: '+diameter2+'/2, h: '+height+', round: '+value_rounded+', center: '+value_center+', fn: '+sides+'})';
  // CylinderGeometry(radiusTop, radiusBottom, height, radiusSegments, heightSegments, openEnded, thetaStart, thetaLength)
  //code = "mm_new_mesh(new THREE.CylinderGeometry( "+diameter1+"/2, "+diameter2+"/2, "+height+", "+sides+ ", 1 ))";
  code = "mm_cylinder("+diameter1+", "+diameter2+", "+height+", "+sides+ ", 1)";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_torus'] = function(block) {
  var value_diameter = Blockly.JavaScript.valueToCode(block, 'diameter', Blockly.JavaScript.ORDER_ATOMIC);
  var value_tube_diameter = Blockly.JavaScript.valueToCode(block, 'tube_diameter', Blockly.JavaScript.ORDER_ATOMIC);
  var value_radial_segments = Blockly.JavaScript.valueToCode(block, 'radial_segments', Blockly.JavaScript.ORDER_ATOMIC);
  var value_tubular_segments = Blockly.JavaScript.valueToCode(block, 'tubular_segments', Blockly.JavaScript.ORDER_ATOMIC);
  var value_arc = Blockly.JavaScript.valueToCode(block, 'arc', Blockly.JavaScript.ORDER_ATOMIC);

  if(value_diameter==""){
    var diameter = "100"; // Default value
  }
  else {
    var diameter = value_diameter;
  }
  if(value_tube_diameter==""){
    var tube_diameter = "10"; // Default value
  }
  else {
    var tube_diameter = value_tube_diameter;
  }
  if(value_radial_segments==""){
    var radial_segments = "16"; // Default value
  }
  else {
    var radial_segments = value_radial_segments;
  }
  if(value_tubular_segments==""){
    var tubular_segments = "16"; // Default value
  }
  else {
    var tubular_segments = value_tubular_segments;
  }
  if(value_arc==""){
    var arc = "2*3.1416"; // Default value
  }
  else {
    var arc = value_arc+"*3.1416/180";
  }
  //var code = 'torus({ri:'+ri+',ro:'+ro+', fni:'+fni+',fno:'+fno+',roti:'+roti+' })';
  //TorusGeometry(radius, tube, radialSegments, tubularSegments, arc)
  code = "mm_new_mesh(new THREE.TorusGeometry( "+diameter+"/2, "+tube_diameter+", "+radial_segments+", "+tubular_segments+", "+arc+" ))";
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_text'] = function(block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  var value_size = Blockly.JavaScript.valueToCode(block, 'size', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  if(value_text==""){
    var text = "'Text'"; // Default value
  }
  else {
    var text = value_text;
  }
  if(value_size==""){
    var size = "100"; // Default value
  }
  else {
    var size = value_size;
  }
  if(value_height==""){
    var height = "10"; // Default value
  }
  else {
    var height = value_height;
  }
  // 12 is the curveSegments parameter. (Default value for now)
  var code = 'mm_text2mesh('+text+', mm_font, '+size+', '+height+', 12)';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_polygon'] = function(block) {
  var text_code = block.getFieldValue('code');
  if(text_code.startsWith("polygon(")){
    code1=text_code.substr(8,text_code.length-10);
  }
  else{
    code1=text_code;
  }
  var code = 'mm_polygon('+code1+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_spiral'] = function(block) {
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var value_segments = Blockly.JavaScript.valueToCode(block, 'segments', Blockly.JavaScript.ORDER_ATOMIC);
  var statements_angle_p = Blockly.JavaScript.statementToCode(block, 'angle_p');
  var chain1 = "";
  if(statements_angle_p != ""){
    chain1 = statements_angle_p.trim();
  }
  var statements_radius_p = Blockly.JavaScript.statementToCode(block, 'radius_p');
  var chain2 = "";
  if(statements_radius_p != ""){
    chain2 = statements_radius_p.trim();
  }  
  //  var value_angle_z = Blockly.JavaScript.valueToCode(block, 'angle_p', Blockly.JavaScript.ORDER_ATOMIC);
  //  var value_radius_z = Blockly.JavaScript.valueToCode(block, 'radius_p', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_spiral('+value_height+', '+value_segments+',\nfunction(p){\n  '+chain1+'\n},\nfunction(p){\n  '+chain2+'\n})';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_extrude_spiral'] = function(block) {
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var value_sections = Blockly.JavaScript.valueToCode(block, 'sections', Blockly.JavaScript.ORDER_ATOMIC);
  var value_polygon = Blockly.JavaScript.valueToCode(block, 'polygon', Blockly.JavaScript.ORDER_ATOMIC);
  var statements_rotations_p = Blockly.JavaScript.statementToCode(block, 'rotations_p');
  var chain1 = "";
  if(statements_rotations_p != ""){
    chain1 = statements_rotations_p.trim();
  }
  var statements_radius_p = Blockly.JavaScript.statementToCode(block, 'radius_p');
  var chain2 = "";
  if(statements_radius_p != ""){
    chain2 = statements_radius_p.trim();
  }  

  var code = 'mm_extrude_spiral('+value_height+', '+value_sections+', '+value_polygon+',\nfunction(p){\n  '+chain1+'\n},\nfunction(p){\n  '+chain2+'\n})';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_extrude'] = function(block) {
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var value_segments = Blockly.JavaScript.valueToCode(block, 'segments', Blockly.JavaScript.ORDER_ATOMIC);
  var value_twist = Blockly.JavaScript.valueToCode(block, 'twist', Blockly.JavaScript.ORDER_ATOMIC);
  var value_polygon = Blockly.JavaScript.valueToCode(block, 'polygon', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_extrude('+value_polygon+', '+value_height+', '+value_segments+', '+value_twist+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_extrude_path'] = function(block) {
  var value_closed = Blockly.JavaScript.valueToCode(block, 'closed', Blockly.JavaScript.ORDER_ATOMIC);
  var value_divisions = Blockly.JavaScript.valueToCode(block, 'divisions', Blockly.JavaScript.ORDER_ATOMIC);
  var value_polygon = Blockly.JavaScript.valueToCode(block, 'polygon', Blockly.JavaScript.ORDER_ATOMIC);
  var value_path = Blockly.JavaScript.valueToCode(block, 'path', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_extrude_path('+value_polygon+', '+value_path+', '+value_divisions+', '+value_closed+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_lathe'] = function(block) {

  var value_closed = Blockly.JavaScript.valueToCode(block, 'closed', Blockly.JavaScript.ORDER_ATOMIC);
  var value_divisions = Blockly.JavaScript.valueToCode(block, 'divisions', Blockly.JavaScript.ORDER_ATOMIC);
  var value_polygon = Blockly.JavaScript.valueToCode(block, 'polygon', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_lathe('+value_polygon+', '+value_divisions+', '+value_closed+')';

  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_scale'] = function(block) {
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var value_coordinates = Blockly.JavaScript.valueToCode(block, 'coordinates', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_object+'\n  .setScale('+value_coordinates+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_translate'] = function(block) {
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var value_coordinates = Blockly.JavaScript.valueToCode(block, 'coordinates', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_object+'\n  .setTranslation('+value_coordinates+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_rotate'] = function(block) {
  var value_object= Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var value_coordinates = Blockly.JavaScript.valueToCode(block, 'coordinates', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_object+'\n  .setRotation('+value_coordinates+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_subdivisions'] = function(block) {
  var value_subdivisions = Blockly.JavaScript.valueToCode(block, 'subdivisions', Blockly.JavaScript.ORDER_ATOMIC);
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_object+'\n  .setSubdivisions('+value_subdivisions+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_setcolor'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_object+'\n  .setColor('+value_color+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_settexture'] = function(block) {
  var value_texture = Blockly.JavaScript.valueToCode(block, 'texture', Blockly.JavaScript.ORDER_ATOMIC);
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_object+'\n  .setTexture('+value_texture+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_setmaterial'] = function(block) {
  var value_transparent = Blockly.JavaScript.valueToCode(block, 'transparent', Blockly.JavaScript.ORDER_ATOMIC);
  var value_opacity = Blockly.JavaScript.valueToCode(block, 'opacity', Blockly.JavaScript.ORDER_ATOMIC);
  var value_material = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_object+'\n  .setMaterial('+value_transparent+', '+value_opacity+', '+value_material+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['physi_creatematerial'] = function(block) {
  var value_mass = Blockly.JavaScript.valueToCode(block, 'mass', Blockly.JavaScript.ORDER_ATOMIC);
  var value_friction = Blockly.JavaScript.valueToCode(block, 'friction', Blockly.JavaScript.ORDER_ATOMIC);
  var value_bounce = Blockly.JavaScript.valueToCode(block, 'bounce', Blockly.JavaScript.ORDER_ATOMIC);
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_new_physi_mesh('+value_object+', '+value_mass+', '+value_friction+', '+value_bounce+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_basicmaterial'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_texture = Blockly.JavaScript.valueToCode(block, 'texture', Blockly.JavaScript.ORDER_ATOMIC);
  var value_wireframe = Blockly.JavaScript.valueToCode(block, 'wireframe', Blockly.JavaScript.ORDER_ATOMIC);

  //t/new THREE.MeshBasicMaterial( { color: 0xFFFFFF, wireframe: false, wireframeLinewidth: 1 } )
  //t/var code = 'new THREE.MeshBasicMaterial( { color: '+value_color+', map: new THREE.TextureLoader().load( '+value_texture+' ), wireframe: false, wireframeLinewidth: 1} )';
  var code = 'new THREE.MeshBasicMaterial( { color: '+value_color+', wireframe: '+value_wireframe+', wireframeLinewidth: 1} )';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_lambertmaterial'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_wireframe = Blockly.JavaScript.valueToCode(block, 'wireframe', Blockly.JavaScript.ORDER_ATOMIC);

  //t/var value_texture = Blockly.JavaScript.valueToCode(block, 'texture', Blockly.JavaScript.ORDER_ATOMIC);
  //t/var code = 'new THREE.MeshLambertMaterial( { color: '+value_color+', map: new THREE.TextureLoader().load( '+value_texture+' ), wireframe: false, wireframeLinewidth: 1} )';
  var code = 'new THREE.MeshLambertMaterial( { color: '+value_color+', wireframe: '+value_wireframe+', wireframeLinewidth: 1} )';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_phongmaterial'] = function(block) {
  var value_color = Blockly.JavaScript.valueToCode(block, 'color', Blockly.JavaScript.ORDER_ATOMIC);
  var value_wireframe = Blockly.JavaScript.valueToCode(block, 'wireframe', Blockly.JavaScript.ORDER_ATOMIC);
  //t/var value_texture = Blockly.JavaScript.valueToCode(block, 'texture', Blockly.JavaScript.ORDER_ATOMIC);
  //t/var code = 'new THREE.MeshPhongMaterial( { color: '+value_color+', map: new THREE.TextureLoader().load( '+value_texture+' ), wireframe: false, wireframeLinewidth: 1} )';
  var code = 'new THREE.MeshPhongMaterial( { color: '+value_color+', wireframe: '+value_wireframe+', wireframeLinewidth: 1} )';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_object_loader'] = function(block) {
  var text_name = block.getFieldValue('name');
  var text_id = block.getFieldValue('id');
  var code = 'mm_assets_objects["'+text_id+'" /* '+text_name+' */].data.clone()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_image_loader'] = function(block) {
  var text_name = block.getFieldValue('name');
  var text_id = block.getFieldValue('id');
  var code = 'mm_assets_textures["'+text_id+'" /* '+text_name+' */].data';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_objloader'] = function(block) {
  var value_url = Blockly.JavaScript.valueToCode(block, 'url', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_objloader('+value_url+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['three_stlloader'] = function(block) {
  var value_url = Blockly.JavaScript.valueToCode(block, 'url', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_stlloader('+value_url+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};
Blockly.JavaScript['three_setshadow'] = function(block) {
  var value_castshadow= Blockly.JavaScript.valueToCode(block, 'castshadow', Blockly.JavaScript.ORDER_ATOMIC);
  var value_receiveshadow= Blockly.JavaScript.valueToCode(block, 'receiveshadow', Blockly.JavaScript.ORDER_ATOMIC);
  var value_block = Blockly.JavaScript.valueToCode(block, 'block', Blockly.JavaScript.ORDER_ATOMIC);
  var code = value_block+'.setShadow('+value_castshadow+','+value_receiveshadow+')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};
Blockly.JavaScript['three_shadowmapenabled'] = function(block) {
  var value_shadowmapenabled= Blockly.JavaScript.valueToCode(block, 'shadowmapenabled', Blockly.JavaScript.ORDER_ATOMIC);
  var value_block = Blockly.JavaScript.valueToCode(block, 'block', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'mm_renderer.shadowMap.enabled='+value_shadowmapenabled+'; mm_renderer.shadowMapSoft = true;\n';

  return code;
};

Blockly.JavaScript['three_gridhelper'] = function(block) {

  var code = 'mm_scene.add(mm_grid);\n';

  return code;
};
Blockly.JavaScript['three_axishelper'] = function(block) {

  var code = 'mm_scene.add(mm_axis);\n';

  return code;
};
Blockly.JavaScript['three_setlightshadow'] = function(block) {
  var value_castshadow= Blockly.JavaScript.valueToCode(block, 'castshadow', Blockly.JavaScript.ORDER_ATOMIC);
  //var value_shadowdarkness= Blockly.JavaScript.valueToCode(block, 'shadowdarkness', Blockly.JavaScript.ORDER_ATOMIC); depreciated in three.js use AmbientLight
  var value_shadowresolution= Blockly.JavaScript.valueToCode(block, 'shadowresolution', Blockly.JavaScript.ORDER_ATOMIC);
  //var value_shadowmapheight= Blockly.JavaScript.valueToCode(block, 'shadowmapheight', Blockly.JavaScript.ORDER_ATOMIC);
  var value_shadowcamerafar= Blockly.JavaScript.valueToCode(block, 'shadowcamerafar', Blockly.JavaScript.ORDER_ATOMIC);
  var value_isdirectional= Blockly.JavaScript.valueToCode(block, 'isdirectional', Blockly.JavaScript.ORDER_ATOMIC);
  var value_shadowcamerasize= Blockly.JavaScript.valueToCode(block, 'shadowcamerasize', Blockly.JavaScript.ORDER_ATOMIC);
  var value_camerahelper= Blockly.JavaScript.valueToCode(block, 'camerahelper', Blockly.JavaScript.ORDER_ATOMIC);
  //var value_shadowcameraright= Blockly.JavaScript.valueToCode(block, 'shadowcameraright', Blockly.JavaScript.ORDER_ATOMIC);
  //var value_shadowcameratop= Blockly.JavaScript.valueToCode(block, 'shadowcameratop', Blockly.JavaScript.ORDER_ATOMIC);
  //var value_shadowcamerabottom= Blockly.JavaScript.valueToCode(block, 'shadowcamerabottom', Blockly.JavaScript.ORDER_ATOMIC);
  var value_block = Blockly.JavaScript.valueToCode(block, 'block', Blockly.JavaScript.ORDER_ATOMIC);
  var code = "mm_set("+value_block+'.setLightShadow('+value_castshadow+','+value_shadowresolution+','+value_shadowcamerafar+','+value_isdirectional+','+value_shadowcamerasize+','+value_camerahelper+'))';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};
Blockly.JavaScript['three_clone'] = function(block) {
  var value_object = Blockly.JavaScript.valueToCode(block, 'object', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = value_object+'.clone()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};
