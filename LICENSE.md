The 3Designer project is Copyright (c) 2016 Juan Carlos Orozco Arena. All rights reserved.
The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2016 MakerMex, SA de CV. All rights reserved.

MakerSCAD software is distributed using GPL 3 Open Source license, see "gpl-3.0.txt" file for the terms of this license.

See also the file "MakerSCAD_Trademark_Policy.pdf" for the terms of use of the MakerSCAD and MakerMex trademarks and logos.

MakerSCAD uses code from the next projects that use MIT License, BSD License and Apache 2.0 License:

- Blockly (Apache 2.0 License)

- threejs (MIT License)

- OpenSCAD-polygon-editor (Public Domain License)

- Polymer (BSD)

- ThreeCGS (MIT License)

- Physijs (MIT License)

- ACE Editor (BSD)

- jszip (MIT License)

- SparkMD5 (MIT License)

- mqttjs (MIT)

- dat.gui (Apache 2.0 License)

- Blocks IDE (Apache 2.0 License)
