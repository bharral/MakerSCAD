PK
     wu�L�dv	   	      mmp{"ui":{}}PK
     wu�L	�
�  �     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_init" id="iOdW(*a,bzf1AwkXmCa^" x="12" y="34"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="variables_set" id="*=|{q+lV_0x,Xv~{B0;K"><field name="VAR">item</field><value name="VALUE"><block type="three_box" id="D._i!t?(SR(j:6f`7[MU"><value name="size"><shadow type="mm_xyz" id="m=)1N2YkC15?-%Rz]K;Z"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="tSB~}rmnK9S7yVAI:U,8"><field name="NUM">1</field></shadow></value></block></value><next><block type="three_scene_add" id="![tVI-L=YRz`~NOsPfK!"><value name="mesh"><shadow type="three_box" id="qQSrEs^S(_`;n5M.s2!y"><value name="size"><shadow type="mm_xyz" id="*k;^3m1Or?M)hgUE+WV#"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="OIrJQS1Bc@0Vppeun9:?"><field name="NUM">1</field></shadow></value></shadow><block type="variables_get" id="M!18sN|Z5AgvI@Kp)|3d"><field name="VAR">item</field></block></value><next><block type="variables_set" id="[:jAG6NkIM;_yq,4#`_="><field name="VAR">angle</field><value name="VALUE"><block type="math_number" id="ydREor{3xQ1PjjXu-Cje"><field name="NUM">0</field></block></value></block></next></block></next></block></next></block><block type="three_animation" id="-8=H]?s|W.;ST/;1/4K4" x="11" y="246"><statement name="st"><block type="variables_set" id="_8oK;2Q8*U7+GJ,)6;zi"><field name="VAR">angle</field><value name="VALUE"><block type="math_arithmetic" id="v%k%*#jRjbzZcRLc+4[#"><field name="OP">ADD</field><value name="A"><shadow type="math_number" id="@;`kLs-*~claWnW_K5O6"><field name="NUM">10</field></shadow><block type="variables_get" id="1B^5No`8)^s_quO(jVJ#"><field name="VAR">angle</field></block></value><value name="B"><shadow type="math_number" id="uOL5(Tmk0j1xQ,_[;1d|"><field name="NUM">1</field></shadow></value></block></value><next><block type="variables_set" id="uq.doc{AIBw]h@Y)!zB#"><field name="VAR">item</field><value name="VALUE"><block type="three_rotate" id="PrYw0pe;v)Wj6cCbqCm,"><value name="coordinates"><shadow type="mm_xyz" id="/*t:.B!i`lXr2#-#@e~!"><field name="X">0</field><field name="Y">0</field><field name="Z">0</field></shadow><block type="mm_list_xyz" id="?7YtzmkFI:3iVi:r|8/v"><value name="X"><block type="variables_get" id="ojl_a,g+w(`m%b+0DLb."><field name="VAR">angle</field></block></value><value name="Y"><block type="math_number" id=",]32LzM,sFwb+:?IKOti"><field name="NUM">0</field></block></value><value name="Z"><block type="math_number" id=";u7AQVKoD1b^UD.QlZ~I"><field name="NUM">0</field></block></value></block></value><value name="object"><shadow type="three_box" id="5ui#y9*WX5-Huu*:5Ui|"><value name="size"><shadow type="mm_xyz" id="4857%g2cQh^BlmqMhIeK"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="?Ha#e!q^n:H*|;K}hx=;"><field name="NUM">1</field></shadow></value></shadow><block type="variables_get" id="{GLX;8BHu`M+w)~3QJRk"><field name="VAR">item</field></block></value></block></value></block></next></block></statement></block></xml>PK
     wu�L�Yȁp  p     jsvar angle;
var item;


mm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
item = mm_new_mesh(new THREE.BoxGeometry( 100, 100, 100, 1, 1, 1 ));
mm_scene.add(item);
angle = 0;

mm_animation = function(){
  var delta = mm_clock.getDelta();
  angle = angle + 1;
  item = item
    .setRotation([angle, 0, 0]);
}PK
     wu�LC���      	   functions{}PK
     wu�L            	   textures/PK
     wu�LC���         textures/metadata.json{}PK
     wu�L               objects/PK
     wu�LC���         objects/metadata.json{}PK 
     wu�L�dv	   	                    mmpPK 
     wu�L	�
�  �               *   xmlPK 
     wu�L�Yȁp  p               ,  jsPK 
     wu�LC���      	             �  functionsPK 
     wu�L            	            �  textures/PK 
     wu�LC���                     textures/metadata.jsonPK 
     wu�L                        B  objects/PK 
     wu�LC���                   h  objects/metadata.jsonPK      �  �    