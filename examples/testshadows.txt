<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="three_box" id="Zq7S6oMFUD-l{@Z,ton=" disabled="true" x="104" y="-33">
    <value name="size">
      <block type="mm_list_xyz" id="B/!W8HJ6(yP+NY`Kx_@o">
        <value name="X">
          <block type="math_number" id="b6}GEY.IeDm;-eDfR)XY">
            <field name="NUM">100</field>
          </block>
        </value>
        <value name="Y">
          <block type="math_number" id="tb|s~+#DO5{I)8rJ*Jeo">
            <field name="NUM">100</field>
          </block>
        </value>
        <value name="Z">
          <block type="math_number" id="BEFj`OZdv@VXC@G9=fQ^">
            <field name="NUM">100</field>
          </block>
        </value>
      </block>
    </value>
  </block>
  <block type="three_light_directional" id="IXTn,CJ7jn{F6K3FJ[E1" disabled="true" x="451" y="130"></block>
  <block type="three_new_scene" id="ffNu#7FKt/a{O,2EB9(v" x="33" y="180">
    <next>
      <block type="three_shadowmapenabled" id="OB.H`h;PRkE.#Ed`U-l]">
        <value name="shadowmapenabled">
          <block type="logic_boolean" id=",LG#zi[)ZDeab3P7U{UD">
            <field name="BOOL">TRUE</field>
          </block>
        </value>
        <next>
          <block type="three_scene_add" id="}W*^s}djn+?2s^Z9{,]=" disabled="true">
            <value name="mesh">
              <block type="three_light_ambient" id="}ER?]y`cii9(,=H5g:{^">
                <value name="color">
                  <block type="colour_picker" id="_1zIjTz:+DRh%GTX;/=w">
                    <field name="COLOUR">#ffffff</field>
                  </block>
                </value>
                <value name="intensity">
                  <block type="math_number" id="Huv|I2%f(v3!^7o2F~*T">
                    <field name="NUM">0.25</field>
                  </block>
                </value>
              </block>
            </value>
            <next>
              <block type="three_scene_add" id="7oDeY}g(IUceiL`YEIFe">
                <value name="mesh">
                  <block type="three_setlightshadow" id="G6)1FSRfzkdGOi+-~RU/">
                    <value name="castshadow">
                      <block type="logic_boolean" id=";;j-q#P|cU=?X5Sg!j:m">
                        <field name="BOOL">TRUE</field>
                      </block>
                    </value>
                    <value name="shadowdarkness">
                      <block type="math_number" id="P2.g43/fMPD-L)]-sH,(">
                        <field name="NUM">0.5</field>
                      </block>
                    </value>
                    <value name="shadowmapwidth">
                      <block type="math_number" id="v]B=k|R^DlU[sp~)F%b(">
                        <field name="NUM">2048</field>
                      </block>
                    </value>
                    <value name="shadowmapheight">
                      <block type="math_number" id="RVc8Se)@q{R5Va-g{h;B">
                        <field name="NUM">2048</field>
                      </block>
                    </value>
                    <value name="shadowcamerafar">
                      <block type="math_number" id=":,vy!j+O-7~|U7;SN/.S">
                        <field name="NUM">2500</field>
                      </block>
                    </value>
                    <value name="isdirectional">
                      <block type="logic_boolean" id="[UeW+NWcuSRf|/]HA2n}">
                        <field name="BOOL">TRUE</field>
                      </block>
                    </value>
                    <value name="shadowcameraleft">
                      <block type="math_number" id="#EqM%8@nNz;^h(R~%9|R">
                        <field name="NUM">-1000</field>
                      </block>
                    </value>
                    <value name="shadowcameraright">
                      <block type="math_number" id="?_.NRiDDA`2-}jhe;!jj">
                        <field name="NUM">1000</field>
                      </block>
                    </value>
                    <value name="shadowcameratop">
                      <block type="math_number" id="L%_0CimqwDf[!qCgvNie">
                        <field name="NUM">-1000</field>
                      </block>
                    </value>
                    <value name="shadowcamerabottom">
                      <block type="math_number" id="C|bTYbloDoks;,yL7,;r">
                        <field name="NUM">1000</field>
                      </block>
                    </value>
                    <value name="block">
                      <block type="three_light_directional" id="7Mkh*kmd1oVnG+cQ16`Q">
                        <value name="color">
                          <block type="colour_picker" id="=9w+:`/@t1[iL`VSai/e">
                            <field name="COLOUR">#c0c0c0</field>
                          </block>
                        </value>
                        <value name="intensity">
                          <block type="math_number" id="z=MX.N(E)Z,7,I:h:!D?">
                            <field name="NUM">1</field>
                          </block>
                        </value>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="three_scene_add" id="`@8ViPQ[upM66|@o0[[X">
                    <value name="mesh">
                      <block type="three_light_point" id="/gqt%LX(2#+VRIWJAOW5">
                        <value name="color">
                          <block type="colour_picker" id="Zv(H7g`{Z|A_=YGS18,J">
                            <field name="COLOUR">#000099</field>
                          </block>
                        </value>
                        <value name="intensity">
                          <block type="math_number" id="5o{wOLp^0b4t+Eq?J=#z">
                            <field name="NUM">100</field>
                          </block>
                        </value>
                        <value name="distance">
                          <block type="math_number" id="CY3R91][u4.1uMHCu8B9">
                            <field name="NUM">1000</field>
                          </block>
                        </value>
                        <value name="decay">
                          <block type="math_number" id="5D=YliVp@ZbbEJhTOI;:">
                            <field name="NUM">50</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="three_scene_add" id="B0~CWWV0+M%F1*C8rmJ?">
                        <value name="mesh">
                          <block type="three_setmaterial" id="s9g__%]#_z-v(G%E4-hl">
                            <value name="material">
                              <block type="three_lambertmaterial" id=")AM6uHEIBsZnUzKs]^a)">
                                <value name="color">
                                  <block type="colour_picker" id="3W5s9cu~kbI1iz.IE=8:">
                                    <field name="COLOUR">#ffffff</field>
                                  </block>
                                </value>
                                <value name="wireframe">
                                  <block type="logic_boolean" id="EEhF~_t~2,%*gSvS8?Z3">
                                    <field name="BOOL">FALSE</field>
                                  </block>
                                </value>
                              </block>
                            </value>
                            <value name="block">
                              <block type="three_translate" id="}X}mtJ8C19{OT:Dt6mqW">
                                <value name="coordinates">
                                  <block type="mm_list_xyz" id="A-U?M~e)Nil/fj:5NYN:">
                                    <value name="X">
                                      <block type="math_number" id="vo5hGn6bs;mAIg_ee/gT">
                                        <field name="NUM">0</field>
                                      </block>
                                    </value>
                                    <value name="Y">
                                      <block type="math_number" id="qP,zc?=x8u#ExEXj!p,3">
                                        <field name="NUM">100</field>
                                      </block>
                                    </value>
                                    <value name="Z">
                                      <block type="math_number" id="^QyOf5uiG8LR;uxCw+}U">
                                        <field name="NUM">1</field>
                                      </block>
                                    </value>
                                  </block>
                                </value>
                                <value name="block">
                                  <block type="three_setshadow" id="K`O~1-#!M36-Vhg[HAGV">
                                    <value name="castshadow">
                                      <block type="logic_boolean" id="C8{q]*@JDWx+fpNI2P~e">
                                        <field name="BOOL">TRUE</field>
                                      </block>
                                    </value>
                                    <value name="receiveshadow">
                                      <block type="logic_boolean" id=",vZ7R87v[hd3hBB6FllQ">
                                        <field name="BOOL">TRUE</field>
                                      </block>
                                    </value>
                                    <value name="block">
                                      <block type="three_sphere" id="Z_*Jo]7Q_tQRL^R#N0Cd">
                                        <value name="diameter">
                                          <block type="math_number" id="zLkYFX-=W]%;{WEcA5b{">
                                            <field name="NUM">100</field>
                                          </block>
                                        </value>
                                      </block>
                                    </value>
                                  </block>
                                </value>
                              </block>
                            </value>
                          </block>
                        </value>
                        <next>
                          <block type="three_scene_add" id="G_kjdU)Ac^OvVfS4*qW2">
                            <value name="mesh">
                              <block type="three_setmaterial" id="b{X-bx|!F~gYwv@R}M2T">
                                <value name="material">
                                  <block type="three_lambertmaterial" id="U)e`[0LLB@0.)3h[}#t9">
                                    <value name="color">
                                      <block type="colour_picker" id="#cQ4EOp^a|UKjWP)W1Z/">
                                        <field name="COLOUR">#ffcc33</field>
                                      </block>
                                    </value>
                                    <value name="wireframe">
                                      <block type="logic_boolean" id="u_fgv_[^Y6fY/(XWhRpP">
                                        <field name="BOOL">FALSE</field>
                                      </block>
                                    </value>
                                  </block>
                                </value>
                                <value name="block">
                                  <block type="three_translate" id="oV=n4npvndd%ooqv)bqn">
                                    <value name="coordinates">
                                      <block type="mm_list_xyz" id="Wj=x^*p~=b_@)ib6ruL[">
                                        <value name="X">
                                          <block type="math_number" id="T`4HHM5BznY(Vv,:pCAD">
                                            <field name="NUM">0</field>
                                          </block>
                                        </value>
                                        <value name="Y">
                                          <block type="math_number" id="WH~]2wa4]o/;B]N-P:{u">
                                            <field name="NUM">-10</field>
                                          </block>
                                        </value>
                                        <value name="Z">
                                          <block type="math_number" id="6-WylC.2P!qWCQ32s;E9">
                                            <field name="NUM">0</field>
                                          </block>
                                        </value>
                                      </block>
                                    </value>
                                    <value name="block">
                                      <block type="three_setshadow" id="c_iQeo~%ZKambwS7%Goz">
                                        <value name="castshadow">
                                          <block type="logic_boolean" id="}KKAWk?Lwvywh`j8YK4g">
                                            <field name="BOOL">FALSE</field>
                                          </block>
                                        </value>
                                        <value name="receiveshadow">
                                          <block type="logic_boolean" id="Jj;8T{Xkb0spi#1P2Z+d">
                                            <field name="BOOL">TRUE</field>
                                          </block>
                                        </value>
                                        <value name="block">
                                          <block type="three_box" id="2uyl]oh5D,?lvX3Ur%[E">
                                            <value name="size">
                                              <block type="mm_list_xyz" id="e[F}6%VscMs`Pz=]Ytc`">
                                                <value name="X">
                                                  <block type="math_number" id="nbH6clV?=n0^DzE)3!ar">
                                                    <field name="NUM">300</field>
                                                  </block>
                                                </value>
                                                <value name="Y">
                                                  <block type="math_number" id="z)iMDKF=?aoiNS]@S[=l">
                                                    <field name="NUM">1</field>
                                                  </block>
                                                </value>
                                                <value name="Z">
                                                  <block type="math_number" id="1,jY[_gYN3];s(YU`l9d">
                                                    <field name="NUM">300</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <value name="segments">
                                              <block type="math_number" id="h9j3,:+s3?*9AvNFnR80">
                                                <field name="NUM">30</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                      </block>
                                    </value>
                                  </block>
                                </value>
                              </block>
                            </value>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </next>
  </block>
</xml>
