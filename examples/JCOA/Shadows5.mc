PK
     Ou�LC���         mmp{}PK
     Ou�L���<  <     xml<xml xmlns="http://www.w3.org/1999/xhtml"><block type="three_init" id="b]*YnkWlDG^~[.oaKSq=" x="9" y="3"><field name="lights">TRUE</field><field name="physijs">FALSE</field><next><block type="three_scene_add" id="fv2ok5le?LDSrE1@*ox%"><value name="mesh"><shadow type="three_box" id=")X+}%-w3i`?6V5BZ8#?j"><value name="size"><shadow type="mm_xyz" id="`jQV3VXp)Wa`9*YZ/vM|"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="b.G?2}0%7YpF8g5z/CYL"><field name="NUM">1</field></shadow></value></shadow><block type="three_translate" id="jJ2twshA88+uo93?]z1)"><value name="coordinates"><shadow type="mm_xyz" id="[k*dbbSR:AfWjriCC3hu"><field name="X">400</field><field name="Y">-400</field><field name="Z">400</field></shadow></value><value name="object"><shadow type="three_box" id="wqa~Z:KS)w^nYcN2Ihf{"><value name="size"><shadow type="mm_xyz" id="~#MSAQ12R~wQQoIC7%bo"><field name="X">100</field><field name="Y">100</field><field name="Z">100</field></shadow></value><value name="segments"><shadow type="math_number" id="}?Yzvut{#/XY?bq]!YVA"><field name="NUM">1</field></shadow></value></shadow><block type="three_light_point" id="a.*7G[~=O5_DNtQAA26."><value name="color"><shadow type="colour_picker" id="1Em~ep0rNto-(jKpP]d+"><field name="COLOUR">#ffffff</field></shadow></value><value name="intensity"><shadow type="math_number" id="t}MjJ,dKMzW~LSuOCWLO"><field name="NUM">50</field></shadow></value><value name="distance"><shadow type="math_number" id="*aeGwh}!et6/_5mJkI7w"><field name="NUM">2000</field></shadow></value><value name="decay"><shadow type="math_number" id="lhv;d|@VAT,5qFmP|*k6"><field name="NUM">10</field></shadow></value></block></value></block></value><next><block type="three_scene_add" id="H)!50~JS%{j%RGaaBcHU"><value name="mesh"><block type="three_extrude_spiral" id="^eFfIkh12@LUjM{[b~])"><value name="height"><block type="math_number" id="xe1=3ye=2hozd*6Kp-0J"><field name="NUM">100</field></block></value><value name="sections"><block type="math_number" id="+lJnRI#j}!n-k+=/~Sk1"><field name="NUM">400</field></block></value><value name="polygon"><block type="three_polygon" id="]*i(oMm_j|/1Hq?W7KIX"><field name="code">[[0,0],[10,0],[0,10]]</field></block></value><statement name="rotations_p"><block type="mm_return" id="UN%LF7=z3a%AUE]KB-aV"><value name="ret"><shadow type="math_number" id="P7b7Hp.H;|J@|Z{@Mniu"><field name="NUM">0</field></shadow><block type="math_arithmetic" id=":F2mf(B~vad93b+Eamf+"><field name="OP">MULTIPLY</field><value name="A"><shadow type="math_number" id="B8fwHb8op-t?f/`+)By1"><field name="NUM">1</field></shadow><block type="mm_var_return" id="g!rePGUPfnL=aH(5Y*KY"><field name="NAME">p</field></block></value><value name="B"><shadow type="math_number" id="CK0kzwp9Ug6FCBVsQ4NM"><field name="NUM">10</field></shadow></value></block></value></block></statement><statement name="radius_p"><block type="mm_return" id="kWkcfmKpSUmIyFR1zQrC"><value name="ret"><shadow type="math_number" id="b;oJ,?XDsaWL_0a%RlmK"><field name="NUM">40</field></shadow></value></block></statement></block></value><next><block type="three_scene_add" id="OkrwbtL+gzx6|m2[[V*("><value name="mesh"><shadow type="three_box" id="?Z^/)U`P`6zMRe3l)g-b"><value name="size"><shadow type="mm_xyz" id="6gptcO2kr-WHylarY8t?"><field name="X">500</field><field name="Y">500</field><field name="Z">5</field></shadow></value><value name="segments"><shadow type="math_number" id="%[m?[]Awg)rEHlo*7maa"><field name="NUM">1</field></shadow></value></shadow></value></block></next></block></next></block></next></block></xml>PK
     Ou�L�kP�  �     jsmm_scene = null;
mm_scene = new THREE.Scene();
mm_scene.add(new THREE.HemisphereLight('#ffffff', '#666666', 1));
mm_scene.add(mm_set(new THREE.PointLight('#ffffff', 50, 2000, 10))
  .setTranslation([400, -400, 400]));
mm_scene.add(mm_extrude_spiral(100, 400, mm_polygon([[0,0],[10,0],[0,10]]),
function(p){
  return (p * 10)
},
function(p){
  return 40
}));
mm_scene.add(mm_new_mesh(new THREE.BoxGeometry( 500, 500, 5, 1, 1, 1 )));
PK
     Ou�L�z.c   c   	   functions{"script1.js":{"hash":"0_d41d8cd98f00b204e9800998ecf8427e","name":"script1.js","data":"","url":""}}PK
     Ou�L            	   textures/PK
     Ou�LC���         textures/metadata.json{}PK
     Ou�L               objects/PK
     Ou�LC���         objects/metadata.json{}PK 
     Ou�LC���                       mmpPK 
     Ou�L���<  <               #   xmlPK 
     Ou�L�kP�  �               �  jsPK 
     Ou�L�z.c   c   	             P  functionsPK 
     Ou�L            	            �  textures/PK 
     Ou�LC���                     textures/metadata.jsonPK 
     Ou�L                        7  objects/PK 
     Ou�LC���                   ]  objects/metadata.jsonPK      �  �    