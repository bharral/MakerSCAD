JCOA
- Optimization Opportunity. All asset dialogs are very similar
and they have the option to load from file and from url.
Reuse, elements, mutators, behaviors, libraries.

- For some reason ace-widget-jcoa is on bower is retrieving the original ace-widget instead
 my modified version which has value: undefined for maxLines and minLines on ace-widget.html

- Add modules menu.

- Add game engine, PlayCanvas.

- Fix Asset Manager. URLs, Mosaic view, load same file two times after deletion.

- Add options to HTML Exporter, example animation or interactive, etc. Include used libraries. Maybe bundle zip file? Or add to webstore?

- When opening asset manager without a block to use the asset I get a can not read _text of undefined ... error.

- Part object does not work well with complex objects (ex. groups with children, grand children, etc)

- Fix wireframe on asset loaded objects. Maybe is because of children not being changed.
- Test textures. Test imports (Asset management)

- Zoom to fit. On main window and on asset manager.

- Mejorar la visualizacion, tomar como referencia 3D Builder de Microsoft.
- Integrar polygon editor sin usar iFrames o ver como usar iFrames en chromeapps
- Improve readability of code. Ex. Function names, add indent to code generators to avoid very long lines of code.
- Hacer manuales y tutoriales de introducción antes de mostrar sistema a Beta testers.

jcoa_three branch
- Interactive asset load, save and load functions (cloud services?)
http://stackoverflow.com/questions/19183180/how-to-save-an-image-to-localstorage-and-display-it-on-the-next-page
Localstorage has a 5Mb size limit so this may not be a good idea. Use file download, upload instead. Package source and assets when saving the file.
- Pending material blocks
- New camera (Maybe just functions to move camera)
- Test animate(delta) function
- Test using a group instead of an empty mesh for loaders. Can a group have a material.
- Free program from external dependencies. Blockly does not load images when there is no internet connection.
- Change menu layout
- Make windows resizable
- Add code window
DONE: - Add file-saver.js to bower. Use "bower init" "bower install --save file-saver.js"
- Material modifiers must apply recursively to a mesh ands its children (or to a group)

- Add image loader (example to load random images from Internet maybe with a theme)
- Add text js block
- Add obj loader
- Add functions to move camera interactively.
v Change modifiers to get the vector first then the mesh.
v Call threejs and ThreeCSG from bower_components
v Add geometry boolean operators using ThreeCGS (part)
- Rounded cube.
- Use degrees for rotation.

- Cube with random cubes and images.

JCOA (March 8, 2016):
- Add js window.
- JavaScript or Blockly mode.
Cloud services:
- Load and save projects.
- Load and create libraries.
__________________________________________
LPA

-make graphics for new blocks
-DONE Create 2D grid (20x20cm)
-DONE configure initial setup to show grid
-Review material blocks to add any important settings.
-Activate shadows
-Make Global helpers block.
-mockup
-test textures
-test import obj, stl
-perspective gimball
-setup defaults
-----------------------------------------------
OLD:
Add jscad export.

Add properties.

Add polygon and extrude blocks. JCOA
Add js editor. JCOA
Add library manager. Upload, dowload libraries. JCOA

Optimizations:
Set up nginx web server. Or use bluehost or similar web hosting service.
Include libraries from external web servers to offload traffic from our server.
