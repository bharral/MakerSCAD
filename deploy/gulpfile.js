//Copyright (c) 2018 Juan Carlos Orozco Arena. 
//The MakerSCAD trademark, name and the block icons are owned and Copyright (c) 2018 MakerMex, SA de CV.
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License V3.0 as published by
//the Free Software Foundation

//Author: Juan Carlos Orozco
//Includes contributions by MakerMex team: Luis Arturo Pacheco

//3D design software based on software distributed with MIT License:
//Blockly
//Threejs
//Polymer

// https://github.com/hparra/gulp-rename

var gulp = require('gulp');

var debug = require('gulp-debug');

var clean = require('gulp-clean');

var dirs = { 	base: '../',
 							dest: '../dist/app' }

gulp.task('clean', function () {
	return gulp.src(['build', 'build2', 'dist'], {read: false})
		.pipe(clean());
});

gulp.task('copy', ['clean'], function () {
  return gulp
    .src(['../images/*',
          '../fonts/*',
          '../bower_components/blockly/media/*',
          //'../bower_components/ace-builds/**',
          '../bower_components/iron-splitter/*',
          '../bower_components/Physijs/**',
          //'../bower_components/three.js/*',
          '../bower_components/OpenSCAD-polygon-editor/*'
				],
				 {base: dirs.base})
    //.pipe(debug({title: 'debug copy:'}))
    .pipe(gulp.dest(dirs.dest));
});

var vulcanize = require('gulp-vulcanize');

gulp.task('vulcanize', function () {
	return gulp.src('../index.html')
		.pipe(vulcanize({
      abspath: '',
			excludes: [],
			stripExcludes: false,
      inlineScripts: true,
      inlineCss: true,
      implicitStrip: true,
      stripComments: true
		}))
    .pipe(debug({title: 'debug vulcanize:'}))
		.pipe(gulp.dest('./build'));
});

//var minifyInline = require('gulp-minify-inline');
var minifyHTML = require('gulp-htmlmin');

gulp.task('minify-html', function() {
  gulp.src('./build/*.html')
		.pipe(minifyHTML({ collapseWhitespace: true, removeComments: true, minifyJS: true }))
    .pipe(gulp.dest(dirs.dest))
});

// https://www.npmjs.com/package/gulp-exec
var exec = require('child_process').exec;

gulp.task('a', ['clean', 'copy', 'vulcanize']);
//gulp.task('b', ['minify-inline']);
gulp.task('b', ['minify-html']);

